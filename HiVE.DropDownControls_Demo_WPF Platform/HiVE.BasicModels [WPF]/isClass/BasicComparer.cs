﻿using System.Collections;

namespace HiVE.BasicModels.isClass
{
    /// <summary>
    /// Used to demonstrate the <see cref="GroupedComboBox.SortComparer"/> property.
    /// </summary>
    public class BasicComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            return -Comparer.Default.Compare(x, y);
        }
    }
}
