﻿namespace HiVE.BasicModels.isClass
{
    public class ImageList :
        System.Windows.Controls.ListBox,
        System.ComponentModel.ISupportInitialize,
        //System.Windows.Controls.Primitives.IContainItemStorage,
        System.Windows.IFrameworkInputElement,
        System.Windows.IInputElement,
        System.Windows.Markup.IAddChild,
        System.Windows.Markup.IQueryAmbient,
        System.Windows.Media.Animation.IAnimatable
    { }
}
