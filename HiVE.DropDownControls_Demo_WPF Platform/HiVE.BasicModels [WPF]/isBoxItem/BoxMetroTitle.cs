﻿using System.Windows.Controls;

namespace HiVE.BasicModels.isBoxItem
{
    public class BoxMetroTitle
    {
        public string ImagePath { get; set; }
        public string Title { get; set; }
        public string Description { get; set; }
        public UserControl UserControlPath { get; set; }
        public string Background { get; set; }

        public BoxMetroTitle() { }

        public BoxMetroTitle(
            string imagePath,
            string title,
            string description,
            UserControl userControlPath,
            string background)
        {
            this.ImagePath = imagePath;
            this.Title = title;
            this.Description = description;
            this.UserControlPath = userControlPath;
            this.Background = background;
        }
    }
}
