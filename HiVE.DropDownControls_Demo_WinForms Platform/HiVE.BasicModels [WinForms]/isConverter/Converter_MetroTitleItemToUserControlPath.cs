﻿using HiVE.BasicModels.isBoxItem;
using System;
using System.Windows.Data;

namespace HiVE.BasicModels.isConverter
{
    public class Converter_MetroTitleItemToUserControlPath : IValueConverter
    {
        public object Convert(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            object returnValue = null;

            try
            {
                if (value != null)
                {
                    var metroTitleItem = (BoxMetroTitle)value;
                    returnValue = metroTitleItem.UserControlPath;
                }
            }
            catch { }

            return returnValue;
        }

        public object ConvertBack(
            object value,
            System.Type targetType,
            object parameter,
            System.Globalization.CultureInfo culture)
        {
            throw new NotImplementedException(
                "No need to convert from UserControlPath back to MetroTitleItem");
        }
    }
}
