﻿using System.ComponentModel;

namespace HiVE.BasicModels.isBoxItem
{
    /// <summary>
    /// BoxSpecialMethods for this [HiVE.DropDownControls - Demo [WPF]] HiVE project.
    /// </summary>
    public static class BoxSpecialMethods
    {
        /// <summary>
        /// Get Special ProductInformation
        /// </summary>
        public enum MethodProductInformation
        {
            [SpecialDescription("HiVE.DropDownControls - Demo [WPF]")]
            [Description("HiVE.DropDownControls - Demo [WPF]")]
            ProductName,
            [SpecialDescription("http://www.HiVE.DropDownControls.Demo.WPF.com/")]
            [Description("www.HiVE.DropDownControls.Demo.WPF.com")]
            ProductWebSite,
            [SpecialDescription("http://HiVE.DropDownControls.Demo.WPF@gmail.com/")]
            [Description("HiVE.DropDownControls.Demo.WPF@gmail.com")]
            ProductEmail,
            [SpecialDescription("http://www.PTP.HiVE.com/HiVE.DropDownControls.Demo.WPF/")]
            [Description("www.PTP.HiVE.com/HiVE.DropDownControls.Demo.WPF")]
            ProductLink,
        }

        public enum MethodNodeListItemDetails
        {
            Group,
            Value,
            Display,
            ToolTip
        }

        public enum MethodNodeListItemDetailsGroup
        {
            Gases,
            Metals,
            Radioactive
        }

        public enum MethodNodeListItemDetailsDisplay
        {
            [SpecialDescription("Lighter than air")]
            Helium,
            [SpecialDescription("Explosive")]
            Hydrogen,
            [SpecialDescription("Vital for animal life")]
            Oxygen,
            [SpecialDescription("Inert")]
            Argon,
            [SpecialDescription("Heavy and metallic")]
            Iron,
            [SpecialDescription("Explodes in water")]
            Lithium,
            [SpecialDescription("Good electrical conductor")]
            Copper,
            [SpecialDescription("Precious metal")]
            Gold,
            [SpecialDescription("Anti-bacterial")]
            Silver,
            [SpecialDescription("Used in fission")]
            Uranium,
            [SpecialDescription("Man-made")]
            Plutonium,
            [SpecialDescription("Used in smoke detectors")]
            Americium,
            [SpecialDescription("Radioactive gas")]
            Radon,

            Deuterium,
            Tritium
        }
    }
}
