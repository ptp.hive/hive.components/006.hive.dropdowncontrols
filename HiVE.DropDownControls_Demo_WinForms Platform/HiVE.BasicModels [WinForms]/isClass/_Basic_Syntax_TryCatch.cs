﻿using HiVE.BasicModels.isBoxItem;
using System;
using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.BasicModels.isClass
{
    internal class _Basic_Syntax_TryCatch
    {
        public _Basic_Syntax_TryCatch()
        {

        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_BasicModels;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsClass;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_BasicModels_IsClass.BasicAssemblyAttributeAccessors).ToString();

        private static TcMethodClassFunctions_HiVE_BasicModels_IsClass_BasicAssemblyAttributeAccessors TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        private void SampleFunctions()
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_MainProject_IsForm_MainForm.DisplayBasicFormDetails).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.ErrorLoadInformationsDatabase.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            //this.Cursor = System.Windows.Input.Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
