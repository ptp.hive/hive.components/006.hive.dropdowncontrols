﻿namespace HiVE.DropDownControls_Demo_WinForms
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.statusStripMain = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabelCopyright = new System.Windows.Forms.ToolStripStatusLabel();
            this.flowLayoutPanelToolStripLogo = new System.Windows.Forms.FlowLayoutPanel();
            this.buttonMainMenu = new System.Windows.Forms.Button();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.labelProductVersionMajorMain = new System.Windows.Forms.Label();
            this.labelProductIsTrialMode = new System.Windows.Forms.Label();
            this.flowLayoutPanelToolStripSpecial = new System.Windows.Forms.FlowLayoutPanel();
            this.panelUserControls = new System.Windows.Forms.Panel();
            this.tabControlMain = new System.Windows.Forms.TabControl();
            this.tabPageDropDownControls = new System.Windows.Forms.TabPage();
            this.ucDropDownControls1 = new HiVE.DropDownControls_Demo_WinForms.isUserControl.ucDropDownControls();
            this.tabPageAbout = new System.Windows.Forms.TabPage();
            this.ucAbout1 = new HiVE.DropDownControls_Demo_WinForms.isUserControl.ucAbout();
            this.tableLayoutPanelMain.SuspendLayout();
            this.statusStripMain.SuspendLayout();
            this.flowLayoutPanelToolStripLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            this.panelUserControls.SuspendLayout();
            this.tabControlMain.SuspendLayout();
            this.tabPageDropDownControls.SuspendLayout();
            this.tabPageAbout.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.AutoSize = true;
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelMain.Controls.Add(this.statusStripMain, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.flowLayoutPanelToolStripLogo, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.flowLayoutPanelToolStripSpecial, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.panelUserControls, 0, 1);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 3;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(884, 521);
            this.tableLayoutPanelMain.TabIndex = 1;
            // 
            // statusStripMain
            // 
            this.tableLayoutPanelMain.SetColumnSpan(this.statusStripMain, 2);
            this.statusStripMain.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabelCopyright});
            this.statusStripMain.Location = new System.Drawing.Point(0, 499);
            this.statusStripMain.Name = "statusStripMain";
            this.statusStripMain.Size = new System.Drawing.Size(884, 22);
            this.statusStripMain.TabIndex = 12;
            this.statusStripMain.Text = "statusStripMainForm";
            // 
            // toolStripStatusLabelCopyright
            // 
            this.toolStripStatusLabelCopyright.Name = "toolStripStatusLabelCopyright";
            this.toolStripStatusLabelCopyright.Size = new System.Drawing.Size(119, 17);
            this.toolStripStatusLabelCopyright.Text = "[AssemblyCopyright]";
            // 
            // flowLayoutPanelToolStripLogo
            // 
            this.flowLayoutPanelToolStripLogo.AutoSize = true;
            this.flowLayoutPanelToolStripLogo.Controls.Add(this.buttonMainMenu);
            this.flowLayoutPanelToolStripLogo.Controls.Add(this.pictureBoxLogo);
            this.flowLayoutPanelToolStripLogo.Controls.Add(this.labelProductVersionMajorMain);
            this.flowLayoutPanelToolStripLogo.Controls.Add(this.labelProductIsTrialMode);
            this.flowLayoutPanelToolStripLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelToolStripLogo.Location = new System.Drawing.Point(3, 3);
            this.flowLayoutPanelToolStripLogo.Margin = new System.Windows.Forms.Padding(3, 3, 3, 8);
            this.flowLayoutPanelToolStripLogo.Name = "flowLayoutPanelToolStripLogo";
            this.flowLayoutPanelToolStripLogo.Padding = new System.Windows.Forms.Padding(5);
            this.flowLayoutPanelToolStripLogo.Size = new System.Drawing.Size(872, 80);
            this.flowLayoutPanelToolStripLogo.TabIndex = 0;
            this.flowLayoutPanelToolStripLogo.WrapContents = false;
            // 
            // buttonMainMenu
            // 
            this.buttonMainMenu.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.buttonMainMenu.AutoSize = true;
            this.buttonMainMenu.BackColor = System.Drawing.Color.Goldenrod;
            this.buttonMainMenu.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.buttonMainMenu.Location = new System.Drawing.Point(8, 22);
            this.buttonMainMenu.MinimumSize = new System.Drawing.Size(0, 36);
            this.buttonMainMenu.Name = "buttonMainMenu";
            this.buttonMainMenu.Size = new System.Drawing.Size(79, 36);
            this.buttonMainMenu.TabIndex = 0;
            this.buttonMainMenu.Text = "Main Menu";
            this.buttonMainMenu.UseVisualStyleBackColor = false;
            this.buttonMainMenu.Click += new System.EventHandler(this.buttonMainMenu_Click);
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.pictureBoxLogo.Image = global::HiVE.DropDownControls_Demo_WinForms.Properties.Resources.Logo_x512_png;
            this.pictureBoxLogo.Location = new System.Drawing.Point(93, 8);
            this.pictureBoxLogo.MaximumSize = new System.Drawing.Size(72, 72);
            this.pictureBoxLogo.MinimumSize = new System.Drawing.Size(64, 64);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(64, 64);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLogo.TabIndex = 1;
            this.pictureBoxLogo.TabStop = false;
            // 
            // labelProductVersionMajorMain
            // 
            this.labelProductVersionMajorMain.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelProductVersionMajorMain.AutoSize = true;
            this.labelProductVersionMajorMain.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelProductVersionMajorMain.ForeColor = System.Drawing.Color.Red;
            this.labelProductVersionMajorMain.Location = new System.Drawing.Point(163, 31);
            this.labelProductVersionMajorMain.Name = "labelProductVersionMajorMain";
            this.labelProductVersionMajorMain.Size = new System.Drawing.Size(182, 17);
            this.labelProductVersionMajorMain.TabIndex = 2;
            this.labelProductVersionMajorMain.Text = "[AssemblyProductMajor]";
            // 
            // labelProductIsTrialMode
            // 
            this.labelProductIsTrialMode.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.labelProductIsTrialMode.AutoSize = true;
            this.labelProductIsTrialMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelProductIsTrialMode.ForeColor = System.Drawing.Color.Blue;
            this.labelProductIsTrialMode.Location = new System.Drawing.Point(351, 33);
            this.labelProductIsTrialMode.Name = "labelProductIsTrialMode";
            this.labelProductIsTrialMode.Size = new System.Drawing.Size(117, 13);
            this.labelProductIsTrialMode.TabIndex = 3;
            this.labelProductIsTrialMode.Text = "ProductIsTrialMode";
            // 
            // flowLayoutPanelToolStripSpecial
            // 
            this.flowLayoutPanelToolStripSpecial.AutoSize = true;
            this.flowLayoutPanelToolStripSpecial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelToolStripSpecial.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanelToolStripSpecial.Location = new System.Drawing.Point(881, 3);
            this.flowLayoutPanelToolStripSpecial.Name = "flowLayoutPanelToolStripSpecial";
            this.flowLayoutPanelToolStripSpecial.Size = new System.Drawing.Size(1, 85);
            this.flowLayoutPanelToolStripSpecial.TabIndex = 1;
            this.flowLayoutPanelToolStripSpecial.WrapContents = false;
            // 
            // panelUserControls
            // 
            this.panelUserControls.AutoSize = true;
            this.panelUserControls.Controls.Add(this.tabControlMain);
            this.panelUserControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panelUserControls.Location = new System.Drawing.Point(3, 94);
            this.panelUserControls.Name = "panelUserControls";
            this.panelUserControls.Size = new System.Drawing.Size(872, 402);
            this.panelUserControls.TabIndex = 3;
            // 
            // tabControlMain
            // 
            this.tabControlMain.Controls.Add(this.tabPageDropDownControls);
            this.tabControlMain.Controls.Add(this.tabPageAbout);
            this.tabControlMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControlMain.Location = new System.Drawing.Point(0, 0);
            this.tabControlMain.Name = "tabControlMain";
            this.tabControlMain.SelectedIndex = 0;
            this.tabControlMain.Size = new System.Drawing.Size(872, 402);
            this.tabControlMain.TabIndex = 0;
            // 
            // tabPageDropDownControls
            // 
            this.tabPageDropDownControls.AutoScroll = true;
            this.tabPageDropDownControls.Controls.Add(this.ucDropDownControls1);
            this.tabPageDropDownControls.Location = new System.Drawing.Point(4, 22);
            this.tabPageDropDownControls.Name = "tabPageDropDownControls";
            this.tabPageDropDownControls.Size = new System.Drawing.Size(864, 376);
            this.tabPageDropDownControls.TabIndex = 4;
            this.tabPageDropDownControls.Text = "DropDown Controls";
            this.tabPageDropDownControls.UseVisualStyleBackColor = true;
            // 
            // ucDropDownControls1
            // 
            this.ucDropDownControls1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ucDropDownControls1.AutoScroll = true;
            this.ucDropDownControls1.AutoSize = true;
            this.ucDropDownControls1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ucDropDownControls1.Location = new System.Drawing.Point(0, 0);
            this.ucDropDownControls1.MaximumSize = new System.Drawing.Size(400, 0);
            this.ucDropDownControls1.Name = "ucDropDownControls1";
            this.ucDropDownControls1.Size = new System.Drawing.Size(400, 422);
            this.ucDropDownControls1.TabIndex = 0;
            // 
            // tabPageAbout
            // 
            this.tabPageAbout.AutoScroll = true;
            this.tabPageAbout.Controls.Add(this.ucAbout1);
            this.tabPageAbout.Location = new System.Drawing.Point(4, 22);
            this.tabPageAbout.Name = "tabPageAbout";
            this.tabPageAbout.Size = new System.Drawing.Size(864, 376);
            this.tabPageAbout.TabIndex = 3;
            this.tabPageAbout.Text = "About";
            this.tabPageAbout.UseVisualStyleBackColor = true;
            // 
            // ucAbout1
            // 
            this.ucAbout1.AutoScroll = true;
            this.ucAbout1.AutoSize = true;
            this.ucAbout1.Cursor = System.Windows.Forms.Cursors.Arrow;
            this.ucAbout1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ucAbout1.Location = new System.Drawing.Point(0, 0);
            this.ucAbout1.Name = "ucAbout1";
            this.ucAbout1.Size = new System.Drawing.Size(864, 376);
            this.ucAbout1.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(884, 521);
            this.Controls.Add(this.tableLayoutPanelMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MinimumSize = new System.Drawing.Size(900, 560);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "HiVE.DropDownControls - Demo [WinForms]";
            this.Load += new System.EventHandler(this.MainForm_Load);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            this.statusStripMain.ResumeLayout(false);
            this.statusStripMain.PerformLayout();
            this.flowLayoutPanelToolStripLogo.ResumeLayout(false);
            this.flowLayoutPanelToolStripLogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            this.panelUserControls.ResumeLayout(false);
            this.tabControlMain.ResumeLayout(false);
            this.tabPageDropDownControls.ResumeLayout(false);
            this.tabPageDropDownControls.PerformLayout();
            this.tabPageAbout.ResumeLayout(false);
            this.tabPageAbout.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.StatusStrip statusStripMain;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabelCopyright;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelToolStripLogo;
        private System.Windows.Forms.Button buttonMainMenu;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Label labelProductVersionMajorMain;
        private System.Windows.Forms.Label labelProductIsTrialMode;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelToolStripSpecial;
        private System.Windows.Forms.Panel panelUserControls;
        private System.Windows.Forms.TabControl tabControlMain;
        private System.Windows.Forms.TabPage tabPageAbout;
        private System.Windows.Forms.TabPage tabPageDropDownControls;
        private isUserControl.ucAbout ucAbout1;
        private isUserControl.ucDropDownControls ucDropDownControls1;
    }
}

