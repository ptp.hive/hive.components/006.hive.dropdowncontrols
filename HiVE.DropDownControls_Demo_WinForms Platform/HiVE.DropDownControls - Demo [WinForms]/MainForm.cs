﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using System;
using System.Windows.Forms;
using static HiVE.BasicModels.isBoxItem.BoxBasicMethods;
using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.DropDownControls_Demo_WinForms
{
    public partial class MainForm : Form
    {
        public MainForm()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_DropDownControls_Demo_WinForms;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsForm;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_DropDownControls_Demo_WinForms_IsForm.MainForm).ToString();

        private static TcMethodClassFunctions_HiVE_DropDownControls_Demo_WinForms_IsForm_MainForm TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        public BoxBasicWindowDetails BasicWindowDetailsItem { get; set; }

        private static BoxBasicWindowDetails DisplayBasicWindowDetails()
        {
            BoxBasicWindowDetails oBoxBasicWindowDetails =
                new BoxBasicWindowDetails(
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    string.Empty,
                    MethodCulture.Default);

            try
            {
                oBoxBasicWindowDetails.MainMenuContent = BasicMethods.BasicMainMenuContent;
                oBoxBasicWindowDetails.MainMenuContent = string.Empty;

                oBoxBasicWindowDetails.AssemblyProductVersionMajor =
                    string.Format(
                        "{0} v{1}",
                        BasicAssemblyAttributeAccessors.EntryAssemblyProduct,
                        BasicAssemblyAttributeAccessors.EntryAssemblyVersionMajor
                        );

                oBoxBasicWindowDetails.AssemblyCopyright = BasicAssemblyAttributeAccessors.EntryAssemblyCopyright;

                oBoxBasicWindowDetails.ProductIsTrialMode = MethodProductTypeTrialMode.Demo.ToSpecialDescriptionString();

                oBoxBasicWindowDetails.BasicCulture = BasicMethods.BasicCulture;
            }
            catch { }

            return oBoxBasicWindowDetails;
        }

        private bool LoadDisplayBasicWindowDetails()
        {
            bool noError = true;

            try
            {
                if (BasicWindowDetailsItem.MainMenuContent != null && BasicWindowDetailsItem.MainMenuContent != "")
                {
                    buttonMainMenu.Visible = true;
                    buttonMainMenu.Text = BasicWindowDetailsItem.MainMenuContent;
                }
                else { buttonMainMenu.Visible = false; }

                labelProductVersionMajorMain.Text = BasicWindowDetailsItem.AssemblyProductVersionMajor;
                labelProductIsTrialMode.Text = BasicWindowDetailsItem.ProductIsTrialMode;
                toolStripStatusLabelCopyright.Text = BasicWindowDetailsItem.AssemblyCopyright;
            }
            catch { }

            return noError;
        }

        private void buttonMainMenu_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch { }
        }

        private void MainForm_Load(object sender, System.EventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_DropDownControls_Demo_WinForms_IsForm_MainForm.ThisForm_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                BasicWindowDetailsItem = DisplayBasicWindowDetails();
                LoadDisplayBasicWindowDetails();
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingWindowError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
