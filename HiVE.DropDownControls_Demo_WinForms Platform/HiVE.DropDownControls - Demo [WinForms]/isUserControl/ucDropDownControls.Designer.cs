﻿namespace HiVE.DropDownControls_Demo_WinForms.isUserControl
{
    partial class ucDropDownControls
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ucDropDownControls));
            this.tableLayoutPanelMain = new System.Windows.Forms.TableLayoutPanel();
            this.labelGroupedComboBox_Editable = new System.Windows.Forms.Label();
            this.labelGroupedComboBox_List = new System.Windows.Forms.Label();
            this.labelComboBox = new System.Windows.Forms.Label();
            this.dataGridViewNormalPlusDropDownControls = new System.Windows.Forms.DataGridView();
            this.ColumnGroupedComboBox = new HiVE.DropDownControls.DropDownControls.GroupedComboBoxColumn();
            this.ColumnComboTreeBox = new HiVE.DropDownControls.DropDownControls.ComboTreeBoxColumn();
            this.labelVisualStyles = new System.Windows.Forms.Label();
            this.flowLayoutPanelVisualStylesIsEnabled = new System.Windows.Forms.FlowLayoutPanel();
            this.radioButtonVisualStyles = new System.Windows.Forms.RadioButton();
            this.radioButtonPlain = new System.Windows.Forms.RadioButton();
            this.labelComboTreeBox_Normal = new System.Windows.Forms.Label();
            this.comboTreeBoxNormal = new HiVE.DropDownControls.ComboTreeBox();
            this.groupedComboBoxList = new HiVE.DropDownControls.GroupedComboBox();
            this.groupedComboBoxEditable = new HiVE.DropDownControls.GroupedComboBox();
            this.comboBoxNormal = new System.Windows.Forms.ComboBox();
            this.labelComboTreeBox_Images = new System.Windows.Forms.Label();
            this.labelComboTreeBox_CheckBoxes = new System.Windows.Forms.Label();
            this.comboTreeBoxCheckboxes = new HiVE.DropDownControls.ComboTreeBox();
            this.comboTreeBoxImages = new HiVE.DropDownControls.ComboTreeBox();
            this.imageListComboTreeBox = new System.Windows.Forms.ImageList(this.components);
            this.comboTreeBoxFlatChecks = new HiVE.DropDownControls.ComboTreeBox();
            this.labelComboTreeBox_CheckBoxList = new System.Windows.Forms.Label();
            this.tableLayoutPanelMain.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNormalPlusDropDownControls)).BeginInit();
            this.flowLayoutPanelVisualStylesIsEnabled.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanelMain
            // 
            this.tableLayoutPanelMain.AutoScroll = true;
            this.tableLayoutPanelMain.AutoSize = true;
            this.tableLayoutPanelMain.ColumnCount = 2;
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanelMain.Controls.Add(this.labelGroupedComboBox_Editable, 0, 2);
            this.tableLayoutPanelMain.Controls.Add(this.labelGroupedComboBox_List, 0, 1);
            this.tableLayoutPanelMain.Controls.Add(this.labelComboBox, 0, 0);
            this.tableLayoutPanelMain.Controls.Add(this.dataGridViewNormalPlusDropDownControls, 0, 8);
            this.tableLayoutPanelMain.Controls.Add(this.labelVisualStyles, 0, 3);
            this.tableLayoutPanelMain.Controls.Add(this.flowLayoutPanelVisualStylesIsEnabled, 1, 3);
            this.tableLayoutPanelMain.Controls.Add(this.labelComboTreeBox_Normal, 0, 4);
            this.tableLayoutPanelMain.Controls.Add(this.comboTreeBoxNormal, 1, 4);
            this.tableLayoutPanelMain.Controls.Add(this.groupedComboBoxList, 1, 1);
            this.tableLayoutPanelMain.Controls.Add(this.groupedComboBoxEditable, 1, 2);
            this.tableLayoutPanelMain.Controls.Add(this.comboBoxNormal, 1, 0);
            this.tableLayoutPanelMain.Controls.Add(this.labelComboTreeBox_Images, 0, 5);
            this.tableLayoutPanelMain.Controls.Add(this.labelComboTreeBox_CheckBoxes, 0, 6);
            this.tableLayoutPanelMain.Controls.Add(this.comboTreeBoxCheckboxes, 1, 6);
            this.tableLayoutPanelMain.Controls.Add(this.comboTreeBoxImages, 1, 5);
            this.tableLayoutPanelMain.Controls.Add(this.comboTreeBoxFlatChecks, 1, 7);
            this.tableLayoutPanelMain.Controls.Add(this.labelComboTreeBox_CheckBoxList, 0, 7);
            this.tableLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanelMain.Name = "tableLayoutPanelMain";
            this.tableLayoutPanelMain.RowCount = 9;
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelMain.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanelMain.Size = new System.Drawing.Size(400, 422);
            this.tableLayoutPanelMain.TabIndex = 0;
            // 
            // labelGroupedComboBox_Editable
            // 
            this.labelGroupedComboBox_Editable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelGroupedComboBox_Editable.AutoSize = true;
            this.labelGroupedComboBox_Editable.Location = new System.Drawing.Point(3, 73);
            this.labelGroupedComboBox_Editable.Name = "labelGroupedComboBox_Editable";
            this.labelGroupedComboBox_Editable.Size = new System.Drawing.Size(194, 13);
            this.labelGroupedComboBox_Editable.TabIndex = 2;
            this.labelGroupedComboBox_Editable.Text = "GroupedComboBox (Editable)";
            // 
            // labelGroupedComboBox_List
            // 
            this.labelGroupedComboBox_List.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelGroupedComboBox_List.AutoSize = true;
            this.labelGroupedComboBox_List.Location = new System.Drawing.Point(3, 41);
            this.labelGroupedComboBox_List.Name = "labelGroupedComboBox_List";
            this.labelGroupedComboBox_List.Size = new System.Drawing.Size(194, 13);
            this.labelGroupedComboBox_List.TabIndex = 1;
            this.labelGroupedComboBox_List.Text = "GroupedComboBox (List)";
            // 
            // labelComboBox
            // 
            this.labelComboBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelComboBox.AutoSize = true;
            this.labelComboBox.Location = new System.Drawing.Point(3, 9);
            this.labelComboBox.Name = "labelComboBox";
            this.labelComboBox.Size = new System.Drawing.Size(194, 13);
            this.labelComboBox.TabIndex = 0;
            this.labelComboBox.Text = "ComboBox (System.Windows.Form)";
            // 
            // dataGridViewNormalPlusDropDownControls
            // 
            this.dataGridViewNormalPlusDropDownControls.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dataGridViewNormalPlusDropDownControls.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridViewNormalPlusDropDownControls.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ColumnGroupedComboBox,
            this.ColumnComboTreeBox});
            this.tableLayoutPanelMain.SetColumnSpan(this.dataGridViewNormalPlusDropDownControls, 2);
            this.dataGridViewNormalPlusDropDownControls.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dataGridViewNormalPlusDropDownControls.Location = new System.Drawing.Point(3, 259);
            this.dataGridViewNormalPlusDropDownControls.MinimumSize = new System.Drawing.Size(320, 160);
            this.dataGridViewNormalPlusDropDownControls.Name = "dataGridViewNormalPlusDropDownControls";
            this.dataGridViewNormalPlusDropDownControls.Size = new System.Drawing.Size(394, 160);
            this.dataGridViewNormalPlusDropDownControls.TabIndex = 2;
            // 
            // ColumnGroupedComboBox
            // 
            this.ColumnGroupedComboBox.HeaderText = "GroupedComboBoxColumn";
            this.ColumnGroupedComboBox.Name = "ColumnGroupedComboBox";
            // 
            // ColumnComboTreeBox
            // 
            this.ColumnComboTreeBox.HeaderText = "ComboTreeBoxColumn";
            this.ColumnComboTreeBox.Name = "ColumnComboTreeBox";
            // 
            // labelVisualStyles
            // 
            this.labelVisualStyles.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVisualStyles.AutoSize = true;
            this.labelVisualStyles.Location = new System.Drawing.Point(3, 105);
            this.labelVisualStyles.Name = "labelVisualStyles";
            this.labelVisualStyles.Size = new System.Drawing.Size(194, 13);
            this.labelVisualStyles.TabIndex = 4;
            this.labelVisualStyles.Text = "Visual Styles";
            // 
            // flowLayoutPanelVisualStylesIsEnabled
            // 
            this.flowLayoutPanelVisualStylesIsEnabled.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanelVisualStylesIsEnabled.AutoScroll = true;
            this.flowLayoutPanelVisualStylesIsEnabled.AutoSize = true;
            this.flowLayoutPanelVisualStylesIsEnabled.Controls.Add(this.radioButtonVisualStyles);
            this.flowLayoutPanelVisualStylesIsEnabled.Controls.Add(this.radioButtonPlain);
            this.flowLayoutPanelVisualStylesIsEnabled.Location = new System.Drawing.Point(203, 100);
            this.flowLayoutPanelVisualStylesIsEnabled.Name = "flowLayoutPanelVisualStylesIsEnabled";
            this.flowLayoutPanelVisualStylesIsEnabled.Size = new System.Drawing.Size(194, 23);
            this.flowLayoutPanelVisualStylesIsEnabled.TabIndex = 5;
            this.flowLayoutPanelVisualStylesIsEnabled.WrapContents = false;
            // 
            // radioButtonVisualStyles
            // 
            this.radioButtonVisualStyles.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonVisualStyles.AutoSize = true;
            this.radioButtonVisualStyles.Checked = true;
            this.radioButtonVisualStyles.Location = new System.Drawing.Point(3, 3);
            this.radioButtonVisualStyles.Name = "radioButtonVisualStyles";
            this.radioButtonVisualStyles.Size = new System.Drawing.Size(39, 17);
            this.radioButtonVisualStyles.TabIndex = 0;
            this.radioButtonVisualStyles.TabStop = true;
            this.radioButtonVisualStyles.Text = "On";
            this.radioButtonVisualStyles.UseVisualStyleBackColor = true;
            this.radioButtonVisualStyles.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // radioButtonPlain
            // 
            this.radioButtonPlain.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.radioButtonPlain.AutoSize = true;
            this.radioButtonPlain.Location = new System.Drawing.Point(48, 3);
            this.radioButtonPlain.Name = "radioButtonPlain";
            this.radioButtonPlain.Size = new System.Drawing.Size(39, 17);
            this.radioButtonPlain.TabIndex = 1;
            this.radioButtonPlain.TabStop = true;
            this.radioButtonPlain.Text = "Off";
            this.radioButtonPlain.UseVisualStyleBackColor = true;
            this.radioButtonPlain.CheckedChanged += new System.EventHandler(this.radioButtons_CheckedChanged);
            // 
            // labelComboTreeBox_Normal
            // 
            this.labelComboTreeBox_Normal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelComboTreeBox_Normal.AutoSize = true;
            this.labelComboTreeBox_Normal.Location = new System.Drawing.Point(3, 137);
            this.labelComboTreeBox_Normal.Name = "labelComboTreeBox_Normal";
            this.labelComboTreeBox_Normal.Size = new System.Drawing.Size(194, 13);
            this.labelComboTreeBox_Normal.TabIndex = 6;
            this.labelComboTreeBox_Normal.Text = "ComboTreeBox (Normal)";
            // 
            // comboTreeBoxNormal
            // 
            this.comboTreeBoxNormal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboTreeBoxNormal.DroppedDown = false;
            this.comboTreeBoxNormal.Location = new System.Drawing.Point(203, 132);
            this.comboTreeBoxNormal.Name = "comboTreeBoxNormal";
            this.comboTreeBoxNormal.SelectedNode = null;
            this.comboTreeBoxNormal.Size = new System.Drawing.Size(194, 23);
            this.comboTreeBoxNormal.TabIndex = 7;
            // 
            // groupedComboBoxList
            // 
            this.groupedComboBoxList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupedComboBoxList.DataSource = null;
            this.groupedComboBoxList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.groupedComboBoxList.FormattingEnabled = true;
            this.groupedComboBoxList.Location = new System.Drawing.Point(203, 37);
            this.groupedComboBoxList.Name = "groupedComboBoxList";
            this.groupedComboBoxList.Size = new System.Drawing.Size(194, 21);
            this.groupedComboBoxList.TabIndex = 1;
            // 
            // groupedComboBoxEditable
            // 
            this.groupedComboBoxEditable.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.groupedComboBoxEditable.DataSource = null;
            this.groupedComboBoxEditable.FormattingEnabled = true;
            this.groupedComboBoxEditable.Location = new System.Drawing.Point(203, 69);
            this.groupedComboBoxEditable.Name = "groupedComboBoxEditable";
            this.groupedComboBoxEditable.Size = new System.Drawing.Size(194, 21);
            this.groupedComboBoxEditable.TabIndex = 8;
            // 
            // comboBoxNormal
            // 
            this.comboBoxNormal.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboBoxNormal.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.comboBoxNormal.FormattingEnabled = true;
            this.comboBoxNormal.Location = new System.Drawing.Point(203, 5);
            this.comboBoxNormal.Name = "comboBoxNormal";
            this.comboBoxNormal.Size = new System.Drawing.Size(194, 21);
            this.comboBoxNormal.Sorted = true;
            this.comboBoxNormal.TabIndex = 9;
            // 
            // labelComboTreeBox_Images
            // 
            this.labelComboTreeBox_Images.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelComboTreeBox_Images.AutoSize = true;
            this.labelComboTreeBox_Images.Location = new System.Drawing.Point(3, 169);
            this.labelComboTreeBox_Images.Name = "labelComboTreeBox_Images";
            this.labelComboTreeBox_Images.Size = new System.Drawing.Size(194, 13);
            this.labelComboTreeBox_Images.TabIndex = 10;
            this.labelComboTreeBox_Images.Text = "ComboTreeBox (Images)";
            // 
            // labelComboTreeBox_CheckBoxes
            // 
            this.labelComboTreeBox_CheckBoxes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelComboTreeBox_CheckBoxes.AutoSize = true;
            this.labelComboTreeBox_CheckBoxes.Location = new System.Drawing.Point(3, 201);
            this.labelComboTreeBox_CheckBoxes.Name = "labelComboTreeBox_CheckBoxes";
            this.labelComboTreeBox_CheckBoxes.Size = new System.Drawing.Size(194, 13);
            this.labelComboTreeBox_CheckBoxes.TabIndex = 11;
            this.labelComboTreeBox_CheckBoxes.Text = "labelComboTreeBox (CheckBoxes)";
            // 
            // comboTreeBoxCheckboxes
            // 
            this.comboTreeBoxCheckboxes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboTreeBoxCheckboxes.DroppedDown = false;
            this.comboTreeBoxCheckboxes.Location = new System.Drawing.Point(203, 196);
            this.comboTreeBoxCheckboxes.Name = "comboTreeBoxCheckboxes";
            this.comboTreeBoxCheckboxes.SelectedNode = null;
            this.comboTreeBoxCheckboxes.ShowCheckBoxes = true;
            this.comboTreeBoxCheckboxes.Size = new System.Drawing.Size(194, 23);
            this.comboTreeBoxCheckboxes.TabIndex = 12;
            // 
            // comboTreeBoxImages
            // 
            this.comboTreeBoxImages.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboTreeBoxImages.DroppedDown = false;
            this.comboTreeBoxImages.ExpandedImageIndex = 1;
            this.comboTreeBoxImages.Images = this.imageListComboTreeBox;
            this.comboTreeBoxImages.Location = new System.Drawing.Point(203, 164);
            this.comboTreeBoxImages.Name = "comboTreeBoxImages";
            this.comboTreeBoxImages.SelectedNode = null;
            this.comboTreeBoxImages.Size = new System.Drawing.Size(194, 23);
            this.comboTreeBoxImages.TabIndex = 13;
            // 
            // imageListComboTreeBox
            // 
            this.imageListComboTreeBox.ImageStream = ((System.Windows.Forms.ImageListStreamer)(resources.GetObject("imageListComboTreeBox.ImageStream")));
            this.imageListComboTreeBox.TransparentColor = System.Drawing.Color.Transparent;
            this.imageListComboTreeBox.Images.SetKeyName(0, "Closed");
            this.imageListComboTreeBox.Images.SetKeyName(1, "Open");
            // 
            // comboTreeBoxFlatChecks
            // 
            this.comboTreeBoxFlatChecks.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.comboTreeBoxFlatChecks.DroppedDown = false;
            this.comboTreeBoxFlatChecks.Location = new System.Drawing.Point(203, 228);
            this.comboTreeBoxFlatChecks.Name = "comboTreeBoxFlatChecks";
            this.comboTreeBoxFlatChecks.SelectedNode = null;
            this.comboTreeBoxFlatChecks.ShowCheckBoxes = true;
            this.comboTreeBoxFlatChecks.Size = new System.Drawing.Size(194, 23);
            this.comboTreeBoxFlatChecks.TabIndex = 14;
            // 
            // labelComboTreeBox_CheckBoxList
            // 
            this.labelComboTreeBox_CheckBoxList.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelComboTreeBox_CheckBoxList.AutoSize = true;
            this.labelComboTreeBox_CheckBoxList.Location = new System.Drawing.Point(3, 233);
            this.labelComboTreeBox_CheckBoxList.Name = "labelComboTreeBox_CheckBoxList";
            this.labelComboTreeBox_CheckBoxList.Size = new System.Drawing.Size(194, 13);
            this.labelComboTreeBox_CheckBoxList.TabIndex = 15;
            this.labelComboTreeBox_CheckBoxList.Text = "labelComboTreeBox (CheckBox List)";
            // 
            // ucDropDownControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.Controls.Add(this.tableLayoutPanelMain);
            this.MaximumSize = new System.Drawing.Size(400, 0);
            this.Name = "ucDropDownControls";
            this.Size = new System.Drawing.Size(400, 422);
            this.Load += new System.EventHandler(this.DropDownControls_Load);
            this.tableLayoutPanelMain.ResumeLayout(false);
            this.tableLayoutPanelMain.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridViewNormalPlusDropDownControls)).EndInit();
            this.flowLayoutPanelVisualStylesIsEnabled.ResumeLayout(false);
            this.flowLayoutPanelVisualStylesIsEnabled.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelMain;
        private System.Windows.Forms.DataGridView dataGridViewNormalPlusDropDownControls;
        private System.Windows.Forms.Label labelComboBox;
        private System.Windows.Forms.Label labelGroupedComboBox_List;
        private DropDownControls.GroupedComboBox groupedComboBoxList;
        private System.Windows.Forms.ImageList imageListComboTreeBox;
        private System.Windows.Forms.Label labelGroupedComboBox_Editable;
        private System.Windows.Forms.Label labelVisualStyles;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelVisualStylesIsEnabled;
        private System.Windows.Forms.RadioButton radioButtonVisualStyles;
        private System.Windows.Forms.RadioButton radioButtonPlain;
        private System.Windows.Forms.Label labelComboTreeBox_Normal;
        private DropDownControls.ComboTreeBox comboTreeBoxNormal;
        private DropDownControls.GroupedComboBox groupedComboBoxEditable;
        private System.Windows.Forms.ComboBox comboBoxNormal;
        private System.Windows.Forms.Label labelComboTreeBox_Images;
        private System.Windows.Forms.Label labelComboTreeBox_CheckBoxes;
        private DropDownControls.ComboTreeBox comboTreeBoxCheckboxes;
        private DropDownControls.ComboTreeBox comboTreeBoxImages;
        private DropDownControls.ComboTreeBox comboTreeBoxFlatChecks;
        private System.Windows.Forms.Label labelComboTreeBox_CheckBoxList;
        private DropDownControls.DropDownControls.GroupedComboBoxColumn ColumnGroupedComboBox;
        private DropDownControls.DropDownControls.ComboTreeBoxColumn ColumnComboTreeBox;
    }
}
