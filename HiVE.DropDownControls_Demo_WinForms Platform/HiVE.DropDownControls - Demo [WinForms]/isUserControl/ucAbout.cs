﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using System;
using System.Windows.Forms;
using static HiVE.BasicModels.isBoxItem.BoxBasicMethods;
using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.DropDownControls_Demo_WinForms.isUserControl
{
    internal partial class ucAbout : UserControl
    {
        public ucAbout()
        {
            InitializeComponent();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_DropDownControls_Demo_WinForms;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_DropDownControls_Demo_WinForms_IsUserControl.About).ToString();

        private TcMethodClassFunctions_HiVE_DropDownControls_Demo_WinForms_IsUserControl_About TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        private void LoadAssemblyAttributeAccessors()
        {
            try
            {
                labelProductVersionMajor.Text = string.Format(
                    "{0} v{1}",
                    BasicAssemblyAttributeAccessors.EntryAssemblyProduct,
                    BasicAssemblyAttributeAccessors.EntryAssemblyVersionMajor
                    );
                labelProductIsTrialMode.Text = MethodProductTypeTrialMode.Demo.ToSpecialDescriptionString();

                labelProduct.Text = BasicAssemblyAttributeAccessors.EntryAssemblyProduct;
                labelVersion.Text = string.Format(
                    "Version {0}",
                    BasicAssemblyAttributeAccessors.EntryAssemblyVersion
                    );
                labelTitle.Text = BasicAssemblyAttributeAccessors.EntryAssemblyTitle;
                labelCopyright.Text = BasicAssemblyAttributeAccessors.EntryAssemblyCopyright;
                labelCompany.Text = BasicAssemblyAttributeAccessors.EntryAssemblyCompany;

                linkLabelCompanyWebSite.Text = MethodCompanyInformation.CompanyWebSite.ToDescriptionString();

                linkLabelCompanyEmail.Text = MethodCompanyInformation.CompanyEmail.ToDescriptionString();

                labelProductTypeLicensed.Text = MethodProductTypeLicensed.PersonalNonCommercial.ToSpecialDescriptionString();

                richTextBoxDescription.Text = BasicAssemblyAttributeAccessors.EntryAssemblyDescription;
            }
            catch { }
        }

        private void RefreshAssemblyAttributeAccessors()
        {
            try
            {
                if (richTextBoxDescription.Text.Trim() == "")
                { richTextBoxDescription.Visible = false; }
                else { richTextBoxDescription.Visible = true; }

                pictureBoxProductLogoLarge.Visible = checkBoxShowProductLogo.Checked;
                pictureBoxCompanyLogoLarge.Visible = checkBoxShowCompanyLogo.Checked;
            }
            catch { }
        }

        private void checkBoxShowProductLogo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                pictureBoxProductLogoLarge.Visible = checkBoxShowProductLogo.Checked;
            }
            catch { }
        }

        private void checkBoxShowCompanyLogo_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                pictureBoxCompanyLogoLarge.Visible = checkBoxShowCompanyLogo.Checked;
            }
            catch { }
        }

        private void About_Load(object sender, EventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_DropDownControls_Demo_WinForms_IsUserControl_About.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                LoadAssemblyAttributeAccessors();
                RefreshAssemblyAttributeAccessors();
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
