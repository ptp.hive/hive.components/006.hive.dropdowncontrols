﻿using HiVE.BasicModels.isBoxItem;
using HiVE.BasicModels.isClass;
using HiVE.DropDownControls;
using System;
using System.Linq;
using System.Windows.Forms;
using static HiVE.BasicModels.isBoxItem.BoxSpecialMethods;
using static HiVE.BasicModels.isClass.TryCatch;

namespace HiVE.DropDownControls_Demo_WinForms.isUserControl
{
    internal partial class ucDropDownControls : UserControl
    {
        public ucDropDownControls()
        {
            InitializeComponent();

            AddNodeCollection();
        }

        /// <summary>
        /// ارتباط با کلاس خطایابی و مدیریت نحوه‌ی نمایش خطا
        /// </summary>
        #region ConnectionTryCatch

        private static BoxTryCatch boxTryCatch { get; set; }

        private static TcMethodAssemblyProduct TryCatchMethodAssemblyProduct = TcMethodAssemblyProduct.HiVE_DropDownControls_Demo_WinForms;
        private static TcMethodTypeTemplate TryCatchMethodTypeTemplate = TcMethodTypeTemplate.IsUserControl;
        private static string TryCatchMethodTypeTemplate_Template =
            EnumExtensions.GetEnumMemberIndex(TcMethodTypeTemplate_HiVE_DropDownControls_Demo_WinForms_IsUserControl.DropDownControls).ToString();

        private TcMethodClassFunctions_HiVE_DropDownControls_Demo_WinForms_IsUserControl_DropDownControls TryCatchMethodClassFunctions_Function { get; set; }

        #endregion

        /// <summary>
        /// Add Dedault Node Collection
        /// </summary>
        private void AddNodeCollection()
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_DropDownControls_Demo_WinForms_IsUserControl_DropDownControls.AddNodeCollection).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                #region Define our collection of list items

                int index = 0;

                // define our collection of list items
                var groupedItems = new[] {
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Helium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Helium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Hydrogen.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Hydrogen.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Oxygen.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Oxygen.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Gases.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Argon.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Argon.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Iron.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Iron.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Lithium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Lithium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Copper.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Copper.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Gold.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Gold.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Metals.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Silver.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Silver.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Uranium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Uranium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Plutonium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Plutonium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Americium.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Americium.ToSpecialDescriptionString()},
                    new {
                        Group = MethodNodeListItemDetailsGroup.Radioactive.ToString(),
                        Value = ++index,
                        Display = MethodNodeListItemDetailsDisplay.Radon.ToString(),
                        ToolTip = MethodNodeListItemDetailsDisplay.Radon.ToSpecialDescriptionString()},
                };

                #endregion

                Action<ComboTreeNodeCollection> addNodesHelper = nodes =>
                {
                    foreach (var grp in groupedItems.GroupBy(x => x.Group))
                    {
                        ComboTreeNode parent = nodes.Add(grp.Key);
                        foreach (var item in grp)
                        {
                            ComboTreeNode child = parent.Nodes.Add(item.Display);
                            child.ToolTip = item.ToolTip;
                        }
                    }
                };

                // anonymous method delegate for transforming the above into nodes
                Action<ComboTreeBox> addNodes = ctb =>
                {
                    addNodesHelper(ctb.Nodes);
                    ctb.Sort();
                    ctb.SelectedNode = ctb.Nodes[0].Nodes[0];
                };

                /// Normal ComboBox
                comboBoxNormal.ValueMember = MethodNodeListItemDetails.Value.ToString();
                comboBoxNormal.DisplayMember = MethodNodeListItemDetails.Display.ToString();
                comboBoxNormal.DataSource = new BindingSource(groupedItems, String.Empty);

                /// Grouped ComboBoxes
                groupedComboBoxList.ValueMember = MethodNodeListItemDetails.Value.ToString();
                groupedComboBoxList.DisplayMember = MethodNodeListItemDetails.Display.ToString();
                groupedComboBoxList.GroupMember = MethodNodeListItemDetails.Group.ToString();
                groupedComboBoxList.DataSource = new BindingSource(groupedItems, String.Empty);

                groupedComboBoxEditable.ValueMember = MethodNodeListItemDetails.Value.ToString();
                groupedComboBoxEditable.DisplayMember = MethodNodeListItemDetails.Display.ToString();
                groupedComboBoxEditable.GroupMember = MethodNodeListItemDetails.Group.ToString();
                groupedComboBoxEditable.SortComparer = new BasicComparer();
                groupedComboBoxEditable.DataSource = new BindingSource(groupedItems, String.Empty);

                /// ComboTreeBoxes
                addNodes(comboTreeBoxNormal);
                addNodes(comboTreeBoxImages);

                addNodes(comboTreeBoxCheckboxes);
                comboTreeBoxCheckboxes.Nodes[0].Nodes[1].Nodes.Add(new ComboTreeNode(MethodNodeListItemDetailsDisplay.Deuterium.ToString()));
                comboTreeBoxCheckboxes.Nodes[0].Nodes[1].Nodes.Add(new ComboTreeNode(MethodNodeListItemDetailsDisplay.Tritium.ToString()));
                comboTreeBoxCheckboxes.CheckedNodes =
                    new ComboTreeNode[] {
                        comboTreeBoxCheckboxes.Nodes[1].Nodes[0],
                        comboTreeBoxCheckboxes.Nodes[1].Nodes[1]
                    };

                foreach (var item in groupedItems)
                {
                    comboTreeBoxFlatChecks.Nodes.Add(item.Display);
                }

                comboTreeBoxFlatChecks.CheckedNodes =
                    new ComboTreeNode[] {
                        comboTreeBoxFlatChecks.Nodes[0],
                        comboTreeBoxFlatChecks.Nodes[1]
                    };

                // DataGridView Columns
                ColumnGroupedComboBox.ValueMember = MethodNodeListItemDetails.Value.ToString();
                ColumnGroupedComboBox.DisplayMember = MethodNodeListItemDetails.Display.ToString();
                ColumnGroupedComboBox.GroupMember = MethodNodeListItemDetails.Group.ToString();
                ColumnGroupedComboBox.DataSource = new BindingSource(groupedItems, String.Empty);

                ColumnComboTreeBox.Images = imageListComboTreeBox;
                ColumnComboTreeBox.ImageIndex = 0;
                ColumnComboTreeBox.ExpandedImageIndex = 1;
                addNodesHelper(ColumnComboTreeBox.Nodes);
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }

        private void radioButtons_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                comboTreeBoxNormal.DrawWithVisualStyles = radioButtonVisualStyles.Checked;
                comboTreeBoxImages.DrawWithVisualStyles = radioButtonVisualStyles.Checked;
                comboTreeBoxCheckboxes.DrawWithVisualStyles = radioButtonVisualStyles.Checked;
                comboTreeBoxFlatChecks.DrawWithVisualStyles = radioButtonVisualStyles.Checked;
            }
            catch { }
        }

        private void DropDownControls_Load(object sender, EventArgs e)
        {
            try
            {
                #region New boxTryCatch

                boxTryCatch =
                   new BoxTryCatch(
                       TryCatchMethodAssemblyProduct,
                       TryCatchMethodTypeTemplate,
                       TryCatchMethodTypeTemplate_Template,

                       EnumExtensions.GetEnumMemberIndex(
                           TcMethodClassFunctions_HiVE_DropDownControls_Demo_WinForms_IsUserControl_DropDownControls.ThisUserControl_Loaded).ToString(),
                       TcMethodFollowingFunction.FollowingFunction1,

                       string.Empty,
                       string.Empty,

                       string.Empty,
                       string.Empty,

                       false,
                       BasicMethods.IsShowFriendlyMessage,
                       false,

                       BasicMethods.BasicMsgBoxCulture);

                #endregion

                radioButtonPlain.Checked = true;
            }
            catch (Exception exc)
            {
                #region Update boxTryCatch

                boxTryCatch.TryCatchMethodFollowingFunction = TcMethodFollowingFunction.FollowingFunction1;
                boxTryCatch.MessageError = exc.Message;
                boxTryCatch.FriendlyMessageError = TcMethodGeneralErrorMessage.LoadingUserControlError.ToDescriptionString();
                boxTryCatch.IsHaveError = true;

                #endregion
            }

            #region TryCatch.GetCEM_Error

            this.Cursor = Cursors.Arrow;
            boxTryCatch.IsShowFinalMessageError = true;
            boxTryCatch = TryCatch.GetCEM_Error(boxTryCatch);

            #endregion
        }
    }
}
