﻿namespace HiVE.DropDownControls_Demo_WinForms.isUserControl
{
    partial class ucAbout
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanelMain = new System.Windows.Forms.FlowLayoutPanel();
            this.tableLayoutPanelProductLogo = new System.Windows.Forms.TableLayoutPanel();
            this.labelProductVersionMajor = new System.Windows.Forms.Label();
            this.labelProductIsTrialMode = new System.Windows.Forms.Label();
            this.labelProduct = new System.Windows.Forms.Label();
            this.labelVersion = new System.Windows.Forms.Label();
            this.labelTitle = new System.Windows.Forms.Label();
            this.labelCopyright = new System.Windows.Forms.Label();
            this.labelCompany = new System.Windows.Forms.Label();
            this.linkLabelCompanyWebSite = new System.Windows.Forms.LinkLabel();
            this.linkLabelCompanyEmail = new System.Windows.Forms.LinkLabel();
            this.labelProductType = new System.Windows.Forms.Label();
            this.labelProductTypeLicensed = new System.Windows.Forms.Label();
            this.richTextBoxDescription = new System.Windows.Forms.RichTextBox();
            this.checkBoxShowProductLogo = new System.Windows.Forms.CheckBox();
            this.checkBoxShowCompanyLogo = new System.Windows.Forms.CheckBox();
            this.pictureBoxLogo = new System.Windows.Forms.PictureBox();
            this.pictureBoxProductLogoLarge = new System.Windows.Forms.PictureBox();
            this.pictureBoxCompanyLogoLarge = new System.Windows.Forms.PictureBox();
            this.flowLayoutPanelMain.SuspendLayout();
            this.tableLayoutPanelProductLogo.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProductLogoLarge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCompanyLogoLarge)).BeginInit();
            this.SuspendLayout();
            // 
            // flowLayoutPanelMain
            // 
            this.flowLayoutPanelMain.AllowDrop = true;
            this.flowLayoutPanelMain.AutoScroll = true;
            this.flowLayoutPanelMain.AutoSize = true;
            this.flowLayoutPanelMain.Controls.Add(this.tableLayoutPanelProductLogo);
            this.flowLayoutPanelMain.Controls.Add(this.labelProduct);
            this.flowLayoutPanelMain.Controls.Add(this.labelVersion);
            this.flowLayoutPanelMain.Controls.Add(this.labelTitle);
            this.flowLayoutPanelMain.Controls.Add(this.labelCopyright);
            this.flowLayoutPanelMain.Controls.Add(this.labelCompany);
            this.flowLayoutPanelMain.Controls.Add(this.linkLabelCompanyWebSite);
            this.flowLayoutPanelMain.Controls.Add(this.linkLabelCompanyEmail);
            this.flowLayoutPanelMain.Controls.Add(this.labelProductType);
            this.flowLayoutPanelMain.Controls.Add(this.labelProductTypeLicensed);
            this.flowLayoutPanelMain.Controls.Add(this.richTextBoxDescription);
            this.flowLayoutPanelMain.Controls.Add(this.checkBoxShowProductLogo);
            this.flowLayoutPanelMain.Controls.Add(this.pictureBoxProductLogoLarge);
            this.flowLayoutPanelMain.Controls.Add(this.checkBoxShowCompanyLogo);
            this.flowLayoutPanelMain.Controls.Add(this.pictureBoxCompanyLogoLarge);
            this.flowLayoutPanelMain.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanelMain.FlowDirection = System.Windows.Forms.FlowDirection.TopDown;
            this.flowLayoutPanelMain.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanelMain.Name = "flowLayoutPanelMain";
            this.flowLayoutPanelMain.Size = new System.Drawing.Size(630, 1583);
            this.flowLayoutPanelMain.TabIndex = 3;
            this.flowLayoutPanelMain.WrapContents = false;
            // 
            // tableLayoutPanelProductLogo
            // 
            this.tableLayoutPanelProductLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanelProductLogo.AutoSize = true;
            this.tableLayoutPanelProductLogo.ColumnCount = 3;
            this.tableLayoutPanelProductLogo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelProductLogo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelProductLogo.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanelProductLogo.Controls.Add(this.pictureBoxLogo, 0, 0);
            this.tableLayoutPanelProductLogo.Controls.Add(this.labelProductVersionMajor, 1, 0);
            this.tableLayoutPanelProductLogo.Controls.Add(this.labelProductIsTrialMode, 2, 0);
            this.tableLayoutPanelProductLogo.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanelProductLogo.Name = "tableLayoutPanelProductLogo";
            this.tableLayoutPanelProductLogo.RowCount = 1;
            this.tableLayoutPanelProductLogo.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanelProductLogo.Size = new System.Drawing.Size(624, 144);
            this.tableLayoutPanelProductLogo.TabIndex = 0;
            // 
            // labelProductVersionMajor
            // 
            this.labelProductVersionMajor.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProductVersionMajor.AutoSize = true;
            this.labelProductVersionMajor.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelProductVersionMajor.ForeColor = System.Drawing.Color.Red;
            this.labelProductVersionMajor.Location = new System.Drawing.Point(137, 59);
            this.labelProductVersionMajor.Margin = new System.Windows.Forms.Padding(5);
            this.labelProductVersionMajor.Name = "labelProductVersionMajor";
            this.labelProductVersionMajor.Padding = new System.Windows.Forms.Padding(3);
            this.labelProductVersionMajor.Size = new System.Drawing.Size(318, 26);
            this.labelProductVersionMajor.TabIndex = 1;
            this.labelProductVersionMajor.Text = "[AssemblyProduct]+[ ]+[Version.Major]";
            // 
            // labelProductIsTrialMode
            // 
            this.labelProductIsTrialMode.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProductIsTrialMode.AutoSize = true;
            this.labelProductIsTrialMode.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelProductIsTrialMode.ForeColor = System.Drawing.Color.Blue;
            this.labelProductIsTrialMode.Location = new System.Drawing.Point(463, 63);
            this.labelProductIsTrialMode.Name = "labelProductIsTrialMode";
            this.labelProductIsTrialMode.Size = new System.Drawing.Size(158, 17);
            this.labelProductIsTrialMode.TabIndex = 2;
            this.labelProductIsTrialMode.Text = "[ProductIsTrialMode]";
            // 
            // labelProduct
            // 
            this.labelProduct.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProduct.AutoSize = true;
            this.labelProduct.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelProduct.Location = new System.Drawing.Point(5, 155);
            this.labelProduct.Margin = new System.Windows.Forms.Padding(5);
            this.labelProduct.Name = "labelProduct";
            this.labelProduct.Size = new System.Drawing.Size(620, 13);
            this.labelProduct.TabIndex = 1;
            this.labelProduct.Text = "[AssemblyProduct]";
            // 
            // labelVersion
            // 
            this.labelVersion.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelVersion.AutoSize = true;
            this.labelVersion.Location = new System.Drawing.Point(5, 178);
            this.labelVersion.Margin = new System.Windows.Forms.Padding(5);
            this.labelVersion.Name = "labelVersion";
            this.labelVersion.Size = new System.Drawing.Size(620, 13);
            this.labelVersion.TabIndex = 2;
            this.labelVersion.Text = "Version [AssemblyVersion]";
            // 
            // labelTitle
            // 
            this.labelTitle.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelTitle.AutoSize = true;
            this.labelTitle.Location = new System.Drawing.Point(5, 201);
            this.labelTitle.Margin = new System.Windows.Forms.Padding(5);
            this.labelTitle.MaximumSize = new System.Drawing.Size(0, 420);
            this.labelTitle.Name = "labelTitle";
            this.labelTitle.Size = new System.Drawing.Size(620, 13);
            this.labelTitle.TabIndex = 3;
            this.labelTitle.Text = "[AssemblyTitle]";
            // 
            // labelCopyright
            // 
            this.labelCopyright.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCopyright.AutoSize = true;
            this.labelCopyright.Location = new System.Drawing.Point(5, 224);
            this.labelCopyright.Margin = new System.Windows.Forms.Padding(5);
            this.labelCopyright.Name = "labelCopyright";
            this.labelCopyright.Size = new System.Drawing.Size(620, 13);
            this.labelCopyright.TabIndex = 4;
            this.labelCopyright.Text = "[AssemblyCopyright]";
            // 
            // labelCompany
            // 
            this.labelCompany.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelCompany.AutoSize = true;
            this.labelCompany.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelCompany.Location = new System.Drawing.Point(5, 247);
            this.labelCompany.Margin = new System.Windows.Forms.Padding(5);
            this.labelCompany.Name = "labelCompany";
            this.labelCompany.Size = new System.Drawing.Size(620, 13);
            this.labelCompany.TabIndex = 5;
            this.labelCompany.Text = "[AssemblyCompany]";
            // 
            // linkLabelCompanyWebSite
            // 
            this.linkLabelCompanyWebSite.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabelCompanyWebSite.AutoSize = true;
            this.linkLabelCompanyWebSite.Location = new System.Drawing.Point(5, 270);
            this.linkLabelCompanyWebSite.Margin = new System.Windows.Forms.Padding(5);
            this.linkLabelCompanyWebSite.Name = "linkLabelCompanyWebSite";
            this.linkLabelCompanyWebSite.Size = new System.Drawing.Size(620, 13);
            this.linkLabelCompanyWebSite.TabIndex = 6;
            this.linkLabelCompanyWebSite.TabStop = true;
            this.linkLabelCompanyWebSite.Text = "[CompanyWebSite]";
            // 
            // linkLabelCompanyEmail
            // 
            this.linkLabelCompanyEmail.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.linkLabelCompanyEmail.AutoSize = true;
            this.linkLabelCompanyEmail.Location = new System.Drawing.Point(5, 293);
            this.linkLabelCompanyEmail.Margin = new System.Windows.Forms.Padding(5);
            this.linkLabelCompanyEmail.Name = "linkLabelCompanyEmail";
            this.linkLabelCompanyEmail.Size = new System.Drawing.Size(620, 13);
            this.linkLabelCompanyEmail.TabIndex = 7;
            this.linkLabelCompanyEmail.TabStop = true;
            this.linkLabelCompanyEmail.Text = "[CompanyEmail]";
            // 
            // labelProductType
            // 
            this.labelProductType.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProductType.AutoSize = true;
            this.labelProductType.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelProductType.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(10)))), ((int)(((byte)(153)))), ((int)(((byte)(255)))));
            this.labelProductType.Location = new System.Drawing.Point(5, 326);
            this.labelProductType.Margin = new System.Windows.Forms.Padding(5, 15, 5, 5);
            this.labelProductType.Name = "labelProductType";
            this.labelProductType.Size = new System.Drawing.Size(620, 13);
            this.labelProductType.TabIndex = 8;
            this.labelProductType.Text = "This Product Is Licenced To:";
            // 
            // labelProductTypeLicensed
            // 
            this.labelProductTypeLicensed.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.labelProductTypeLicensed.AutoSize = true;
            this.labelProductTypeLicensed.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(178)));
            this.labelProductTypeLicensed.Location = new System.Drawing.Point(5, 349);
            this.labelProductTypeLicensed.Margin = new System.Windows.Forms.Padding(5);
            this.labelProductTypeLicensed.Name = "labelProductTypeLicensed";
            this.labelProductTypeLicensed.Size = new System.Drawing.Size(620, 13);
            this.labelProductTypeLicensed.TabIndex = 9;
            this.labelProductTypeLicensed.Text = "[ProductTypeLicensed]";
            // 
            // richTextBoxDescription
            // 
            this.richTextBoxDescription.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.richTextBoxDescription.Location = new System.Drawing.Point(5, 382);
            this.richTextBoxDescription.Margin = new System.Windows.Forms.Padding(5, 15, 5, 5);
            this.richTextBoxDescription.Name = "richTextBoxDescription";
            this.richTextBoxDescription.ReadOnly = true;
            this.richTextBoxDescription.Size = new System.Drawing.Size(620, 96);
            this.richTextBoxDescription.TabIndex = 15;
            this.richTextBoxDescription.Text = "[AssemblyDescription]";
            // 
            // checkBoxShowProductLogo
            // 
            this.checkBoxShowProductLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxShowProductLogo.AutoSize = true;
            this.checkBoxShowProductLogo.Location = new System.Drawing.Point(5, 498);
            this.checkBoxShowProductLogo.Margin = new System.Windows.Forms.Padding(5, 15, 5, 5);
            this.checkBoxShowProductLogo.Name = "checkBoxShowProductLogo";
            this.checkBoxShowProductLogo.Size = new System.Drawing.Size(620, 17);
            this.checkBoxShowProductLogo.TabIndex = 11;
            this.checkBoxShowProductLogo.Text = "Show Product Logo";
            this.checkBoxShowProductLogo.UseVisualStyleBackColor = true;
            this.checkBoxShowProductLogo.CheckedChanged += new System.EventHandler(this.checkBoxShowProductLogo_CheckedChanged);
            // 
            // checkBoxShowCompanyLogo
            // 
            this.checkBoxShowCompanyLogo.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.checkBoxShowCompanyLogo.AutoSize = true;
            this.checkBoxShowCompanyLogo.Location = new System.Drawing.Point(5, 1043);
            this.checkBoxShowCompanyLogo.Margin = new System.Windows.Forms.Padding(5);
            this.checkBoxShowCompanyLogo.Name = "checkBoxShowCompanyLogo";
            this.checkBoxShowCompanyLogo.Size = new System.Drawing.Size(620, 17);
            this.checkBoxShowCompanyLogo.TabIndex = 13;
            this.checkBoxShowCompanyLogo.Text = "Show Company Logo";
            this.checkBoxShowCompanyLogo.UseVisualStyleBackColor = true;
            this.checkBoxShowCompanyLogo.CheckedChanged += new System.EventHandler(this.checkBoxShowCompanyLogo_CheckedChanged);
            // 
            // pictureBoxLogo
            // 
            this.pictureBoxLogo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxLogo.Image = global::HiVE.DropDownControls_Demo_WinForms.Properties.Resources.Logo_x512_png;
            this.pictureBoxLogo.Location = new System.Drawing.Point(2, 8);
            this.pictureBoxLogo.Margin = new System.Windows.Forms.Padding(2, 8, 2, 8);
            this.pictureBoxLogo.MaximumSize = new System.Drawing.Size(128, 128);
            this.pictureBoxLogo.MinimumSize = new System.Drawing.Size(128, 128);
            this.pictureBoxLogo.Name = "pictureBoxLogo";
            this.pictureBoxLogo.Size = new System.Drawing.Size(128, 128);
            this.pictureBoxLogo.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxLogo.TabIndex = 0;
            this.pictureBoxLogo.TabStop = false;
            // 
            // pictureBoxProductLogoLarge
            // 
            this.pictureBoxProductLogoLarge.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxProductLogoLarge.Image = global::HiVE.DropDownControls_Demo_WinForms.Properties.Resources.Logo_x512_png;
            this.pictureBoxProductLogoLarge.Location = new System.Drawing.Point(3, 523);
            this.pictureBoxProductLogoLarge.MaximumSize = new System.Drawing.Size(512, 512);
            this.pictureBoxProductLogoLarge.MinimumSize = new System.Drawing.Size(512, 512);
            this.pictureBoxProductLogoLarge.Name = "pictureBoxProductLogoLarge";
            this.pictureBoxProductLogoLarge.Size = new System.Drawing.Size(512, 512);
            this.pictureBoxProductLogoLarge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxProductLogoLarge.TabIndex = 12;
            this.pictureBoxProductLogoLarge.TabStop = false;
            // 
            // pictureBoxCompanyLogoLarge
            // 
            this.pictureBoxCompanyLogoLarge.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.pictureBoxCompanyLogoLarge.Image = global::HiVE.DropDownControls_Demo_WinForms.Properties.Resources.HiVE_Company_Logo_x512;
            this.pictureBoxCompanyLogoLarge.Location = new System.Drawing.Point(3, 1068);
            this.pictureBoxCompanyLogoLarge.MaximumSize = new System.Drawing.Size(512, 512);
            this.pictureBoxCompanyLogoLarge.MinimumSize = new System.Drawing.Size(512, 512);
            this.pictureBoxCompanyLogoLarge.Name = "pictureBoxCompanyLogoLarge";
            this.pictureBoxCompanyLogoLarge.Size = new System.Drawing.Size(512, 512);
            this.pictureBoxCompanyLogoLarge.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxCompanyLogoLarge.TabIndex = 14;
            this.pictureBoxCompanyLogoLarge.TabStop = false;
            // 
            // ucAbout
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoScroll = true;
            this.AutoSize = true;
            this.Controls.Add(this.flowLayoutPanelMain);
            this.Name = "ucAbout";
            this.Size = new System.Drawing.Size(630, 1583);
            this.Load += new System.EventHandler(this.About_Load);
            this.flowLayoutPanelMain.ResumeLayout(false);
            this.flowLayoutPanelMain.PerformLayout();
            this.tableLayoutPanelProductLogo.ResumeLayout(false);
            this.tableLayoutPanelProductLogo.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxLogo)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxProductLogoLarge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxCompanyLogoLarge)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanelMain;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanelProductLogo;
        private System.Windows.Forms.PictureBox pictureBoxLogo;
        private System.Windows.Forms.Label labelProductVersionMajor;
        private System.Windows.Forms.Label labelProductIsTrialMode;
        private System.Windows.Forms.Label labelProduct;
        private System.Windows.Forms.Label labelVersion;
        private System.Windows.Forms.Label labelTitle;
        private System.Windows.Forms.Label labelCopyright;
        private System.Windows.Forms.Label labelCompany;
        private System.Windows.Forms.LinkLabel linkLabelCompanyWebSite;
        private System.Windows.Forms.LinkLabel linkLabelCompanyEmail;
        private System.Windows.Forms.Label labelProductType;
        private System.Windows.Forms.Label labelProductTypeLicensed;
        private System.Windows.Forms.RichTextBox richTextBoxDescription;
        private System.Windows.Forms.CheckBox checkBoxShowProductLogo;
        private System.Windows.Forms.PictureBox pictureBoxProductLogoLarge;
        private System.Windows.Forms.CheckBox checkBoxShowCompanyLogo;
        private System.Windows.Forms.PictureBox pictureBoxCompanyLogoLarge;
    }
}
